import numpy as np
import mlp
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


def load_file(filename:str) -> list:
    return np.loadtxt(filename, dtype=int, delimiter=' ')


def noise(arr:list, error:float) -> list:
    np.random.seed()
    noise =  np.random.normal(loc=0, scale=3, size=(10, 10))
    # print(np.array2string(noise, max_line_width=90, precision=2, separator=' '))
    # print()
    return np.array(np.rint(np.clip((arr + noise * error), 0, 1)), dtype=int)

def make_output_array(letter:str, target_letters:list):
    ret = np.zeros(len(target_letters), dtype=int)
    if letter in target_letters:
        ret[target_letters.index(letter)] = 1
    else:
        ret[-1] = 1
    
    return ret

def make_samples(n_samples:int, target_letters:list, all_letters:list, noise_error:float = None):
    x, y = [], []
    
    # target_letters = ['B']
    target_letters_dict = {}
    all_letters_dict = {}
    for letter in target_letters:
        target_letters_dict[letter] = load_file('letter' + letter + '.txt')
    for letter in all_letters:
        all_letters_dict[letter] = load_file('letter' + letter + '.txt')

  
    for _ in range(n_samples):
        np.random.seed()
        ind = np.random.randint(0, len(all_letters)-1)
        letter = all_letters[int(ind)]
        y.append(make_output_array(letter, target_letters))

        if noise_error != None: 
             x_val = noise(all_letters_dict[letter], noise_error)
        else:
            x_val = all_letters_dict[letter]
        x.append(x_val.flatten())

    return np.array(x), np.array(y)


# ## 1. Loading dataset
# The dataset utilzied for this example can be downloaded from [http://deeplearning.net/data/mnist/mnist.pkl.gz](http://deeplearning.net/data/mnist/mnist.pkl.gz) and consist of a subset (20k examples) of the famous [MNIST dataset](https://en.wikipedia.org/wiki/MNIST_database). 

# In[2]:

# # Download MNIST data if needed  
# mnist_filename = 'mnist.pkl.gz'
# if not os.path.exists(mnist_filename):    
#     ulr_mnist = 'https://github.com/mnielsen/neural-networks-and-deep-learning/blob/master/data/mnist.pkl.gz'
#     # ulr_mnist = 'http://deeplearning.net/data/mnist/mnist.pkl.gz'
#     urllib.request.urlretrieve(ulr_mnist, mnist_filename)

# # As 'mnist.pkl.gz' was created in Python2, 'latin1' encoding is needed to loaded in Python3
# with gzip.open(mnist_filename, 'rb') as f:
#     train_set, valid_set, test_set = pickle.load(f, encoding='latin1')


# The dataset contains 70K examples divided as: 50k for training, 10k for validation and 10k for testing.
# Each example is a 28x28 pixel grayimages containing a digit. Some examples of the database:

# In[3]:

# samples 
target_letters = ['B', 'E', 'L', '_']
all_letters = ['B', 'E', 'L', 'A', 'R', 'V', 'Y']

sizes = [100, 100, 4]
iterations = 100
samples = 100


x, y = make_samples(samples, target_letters, all_letters)
# x, y = make_samples(samples, target_letters, all_letters, 0.1)

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=20)


# Plot random examples
examples = np.random.randint(len(x_train), size=20)
n_examples = len(examples)
plt.figure()
for ix_example in range(n_examples):
    tmp = np.reshape(x_train[examples[ix_example],:], [10,10])
    ax = plt.subplot(4,5, ix_example + 1)
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    plt.title(str(y_train[examples[ix_example]]))
    plt.imshow(tmp, cmap='gray')


# For sake of time, the MLP is trained with the validation set (10K examples); testing is performed with the test set (10K examples)

# In[4]:


# Training data
train_X = x_train
train_y = y_train 
print('Shape of training set: ' + str(train_X.shape))

# change y [1D] to Y [2D] sparse array coding class
n_examples = len(y_train)
labels = np.unique(y_train, axis=0)[::-1]
train_Y = np.zeros((n_examples, len(labels)))
for ix_label in range(len(labels)):
    # Find examples with with a Label = lables(ix_label)
    ix_tmp = np.where(train_y == labels[ix_label])[0]
    train_Y[ix_tmp, ix_label] = 1




# Test data
test_X = x_test
test_y = y_test 
print('Shape of test set: ' + str(test_X.shape))


examples = np.random.randint(len(test_y), size=10)

# change y [1D] to Y [2D] sparse array coding class
n_examples = len(test_y)
# labels = np.unique(test_y, axis=0)[::-1]
test_Y = np.zeros((n_examples, len(labels)))
for ix_label in range(len(labels)):
    # Find examples with with a Label = lables(ix_label)
    ix_tmp = np.where(test_y == labels[ix_label])[0]
    test_Y[ix_tmp, ix_label] = 1


# ## 2. Parameters of MLP
#  * __Number of layers__ : 4 (input, hidden1, hidden2 output)
#  * __Elements in layers__ : [784, 25, 10, 10]   
#  * __Activation function__ : Rectified Linear function
#  * __Regularization parameter__ : 1 

# ## 3. Creating MLP object 

# In[5]:


# Creating the MLP object initialize the weights
mlp_classifier = mlp.Mlp(size_layers = sizes,
                         act_funct   = 'relu',
                         reg_lambda  = 0,
                         bias_flag   = True)
print(mlp_classifier)


# ## 4. Training MLP object

# In[6]:


# Training with Backpropagation and 400 iterations
loss = np.zeros([iterations,1])


# тут была ошибка (для обучения нужно использовать x_train, y_train)
for ix in range(iterations):
    mlp_classifier.train(x_train, y_train, 1)
    Y_hat = mlp_classifier.predict(x_train)
    y_tmp = np.argmax(Y_hat, axis=1)
    y_hat = labels[y_tmp]
    loss[ix] = (0.5)*np.square(y_hat - y_train).mean()

# Ploting loss vs iterations
plt.figure()
ix = np.arange(iterations)
plt.plot(ix, loss)

# Training Accuracy
Y_hat = mlp_classifier.predict(train_X)
y_tmp = np.argmax(Y_hat, axis=1)
y_hat = labels[y_tmp]

acc = np.mean(1 * (y_hat == train_y))
print('Training Accuracy: ' + str(acc*100))


# ## 5. Testing MLP

# In[7]:


# Test Accuracy
Y_hat = mlp_classifier.predict(test_X)
y_tmp = np.argmax(Y_hat, axis=1)
y_hat = labels[y_tmp]

acc = np.mean(1 * (y_hat == test_y))
print('Testing Accuracy: ' + str(acc*100))  


# In[8]:


print(test_X.shape)
ix_example = 1
tmp = np.reshape(test_X[examples[ix_example],:], [10,10])


# In[9]:


# Some test samples, [T]rue labels and [P]redicted labels
examples = np.random.randint(len(x_test), size=10)
n_examples = len(examples)
plt.figure()
for ix_example in range(n_examples):
    tmp = np.reshape(x_test[examples[ix_example],:], [10,10])
    ax = plt.subplot(2,5, ix_example + 1)
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    plt.title('T'+ str(test_y[examples[ix_example]]) + ', P' + str(y_hat[examples[ix_example]]))
    plt.imshow(tmp, cmap='gray')
    


# ## 6.  Plotting some weights
# #### A. Weights from Input layer to Hidden layer 1

# In[10]:


# веса, скрытый слой 100 нейронов [100, 100*, 4]
w1 = mlp_classifier.theta_weights[0][:,1:]
plt.figure()
for ix_w in range(100):
    tmp = np.reshape(w1[ix_w,:], [10,10])
    ax = plt.subplot(10, 10, ix_w + 1)
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    plt.title(str(ix_w))
    plt.imshow(1- tmp, cmap='gray')


# #### B. Weights from Hidden layer 1 to Hidden layer 2

# In[11]:

# веса, выходной слой 4 нейрона [100, 100, 4*]
w2 =  mlp_classifier.theta_weights[1][:,1:]
plt.figure()
for ix_w in range(4):
    tmp = np.reshape(w2[ix_w,:], [10,10])
    ax = plt.subplot(1,4, ix_w + 1)
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    plt.title(str(ix_w))
    plt.imshow(1- tmp, cmap='gray')


# веса, скрытый слой 200 нейронов [100, 200*, 4]
# w2 =  mlp_classifier.theta_weights[1][:,1:]
# plt.figure()
# for ix_w in range(200):
#     tmp = np.reshape(w2[ix_w,:], [10,10])
#     ax = plt.subplot(4,5, ix_w + 1)
#     ax.set_yticklabels([])
#     ax.set_xticklabels([])
#     plt.title(str(ix_w))
#     plt.imshow(1- tmp, cmap='gray')


# #### C. Weights from Hidden layer 2 to Output layer

# In[12]:


# веса, выходной слой 4 нейрона [100, 200, 4]
# w3 =  mlp_classifier.theta_weights[2][:,1:]
# plt.figure()
# for ix_w in range(4):
#     tmp = np.reshape(w3[ix_w,:], [4,5])
#     ax = plt.subplot(1,4, ix_w + 1)
#     ax.set_yticklabels([])
#     ax.set_xticklabels([])
#     plt.title(str(ix_w))
#     plt.imshow(1- tmp, cmap='gray')


# In[ ]:


plt.show()

